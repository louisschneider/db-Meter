/**
  ******************************************************************************
  * File Name          : freertos.c
  * Description        : Code for freertos applications
  ******************************************************************************
  * This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * Copyright (c) 2017 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "task.h"
#include "cmsis_os.h"

/* USER CODE BEGIN Includes */     
#include "../LIBS/WS2813.h"
#include "dfsdm.h"
#include "usart.h"
#include <string.h>
#include <stdio.h>
#include <math.h>
//#define KARR
/* USER CODE END Includes */

/* Variables -----------------------------------------------------------------*/
osThreadId defaultTaskHandle;

/* USER CODE BEGIN Variables */
#define BUFFLENGHT 128
static int32_t __audioBuffer[BUFFLENGHT] ;
TaskHandle_t micTask;
QueueHandle_t micData;
static char usartBuffer[24];
/* USER CODE END Variables */

/* Function prototypes -------------------------------------------------------*/
void StartDefaultTask(void const * argument);

void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */

/* USER CODE BEGIN FunctionPrototypes */

void vMicTask(void* param);
/* USER CODE END FunctionPrototypes */

/* Hook prototypes */

/* Init FreeRTOS */

void MX_FREERTOS_Init(void) {
  /* USER CODE BEGIN Init */
  ws2813_init_tim(&htim3, TIM_CHANNEL_1, 8);
  /* USER CODE END Init */

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */



  /* USER CODE END RTOS_TIMERS */

  /* Create the thread(s) */
  /* definition and creation of defaultTask */
  osThreadDef(defaultTask, StartDefaultTask, osPriorityNormal, 0, 128);
  defaultTaskHandle = osThreadCreate(osThread(defaultTask), NULL);

  /* USER CODE BEGIN RTOS_THREADS */
  xTaskCreate(vMicTask, "MicTask", 100, NULL, 2, &micTask);
  /* USER CODE END RTOS_THREADS */

  /* USER CODE BEGIN RTOS_QUEUES */
  micData = xQueueCreate(10, sizeof(int64_t));
  /* USER CODE END RTOS_QUEUES */
}

/* StartDefaultTask function */
void StartDefaultTask(void const * argument)
{

  /* USER CODE BEGIN StartDefaultTask */
  /* Infinite loop */
  for(;;)
  {
    osDelay(1);
  }
  /* USER CODE END StartDefaultTask */
}

/* USER CODE BEGIN Application */
void vMicTask(void* param){
	//init
	WS2813_COLOR color_orange={.rgb={.r=35, .g=12, .b=0}};
	WS2813_COLOR color_off={.rgb={.r=0,.g=0,.b=25}};
	int noLED;
	HAL_DFSDM_FilterRegularStart_DMA(&hdfsdm1_filter0, __audioBuffer, BUFFLENGHT);

	while(1){
		int32_t ampli;
		double decibels;
		if(xQueueReceive(micData, &ampli, pdMS_TO_TICKS(10)) == pdTRUE){

			snprintf(usartBuffer, 24, "%ld\n", ampli);
			HAL_UART_Transmit(&huart2, usartBuffer, strlen(usartBuffer), 100);

			if(ampli <= 20000){
				noLED = 0;
			}else if(ampli > 20000 && ampli <= 100000){
				noLED = 1;
			}else if(ampli > 100000 && ampli <= 200000){
				noLED = 2;
			}else if(ampli > 200000 && ampli <= 400000){
				noLED = 3;
			}else if(ampli > 400000 && ampli <= 600000){
				noLED = 4;
			}else if(ampli > 600000 && ampli <= 1000000){
				noLED = 5;
			}else if(ampli > 1000000 && ampli <= 1500000){
				noLED = 6;
			}else if(ampli > 1500000 ){
				noLED = 7;
			}


			for(int i = 0; i < 8; i++){

				if(i <= noLED){
					ws2813_update_led(i,color_orange);
				}else{
					ws2813_update_led(i,color_off);
				}
			}
			ws2813_start_transmission_tim();
		}
	}
}

void HAL_DFSDM_FilterRegConvCpltCallback(DFSDM_Filter_HandleTypeDef *hdfsdm_filter){
	int index;
	int32_t ampli = 0 ;

	int64_t mini = 9999999;
	int64_t max = -99999999;

	//Echantillonnage du signal capte de longueur BUFFLEN
	for(index = 0 ; index < BUFFLENGHT ; index++){
		if(mini > __audioBuffer[index]){
			mini = __audioBuffer[index];
		}

		if(max < __audioBuffer[index]){
			max = __audioBuffer[index];
		}

	}

	ampli = max - mini;



	xQueueSendFromISR(micData, &ampli, NULL);
}
/**
 * @brief Fonction d'appel du timer d'animation
 *
 * Cette fonction exécute un chenillard "K2000"
 * #Nostalgie #Annees80
 *
 *
 */

/* USER CODE END Application */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
